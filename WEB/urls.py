from . import views
from django.urls import path

urlpatterns = [
    path('', views.index, name='index'),
    path('registration', views.registration, name='registration'),
    path('createUser', views.createUser, name='createUser'),
    path('login', views.login, name='login'),
    path('logout', views.logout, name='logout'),
    path('activate/<int:id>', views.activate, name='activate'),
    path('deactivate/<int:id>', views.deactivate, name='deactivate')
]
