from django.shortcuts import render
from django.contrib.auth.models import User, auth
# Create your views here.


def index(request):
    createAdmin()
    return render(request, 'index.html')


def createAdmin():
    if not User.objects.filter(username='admin@admin').exists():
        createadmin = User.objects.create_user(
            username='admin@admin', first_name='admin', password='admin')


def registration(request):
    return render(request, 'Registration.html')


def createUser(request):
    email = request.POST['email']
    password = request.POST['password']
    userType = request.POST['userType']
    if User.objects.filter(username=email).exists():
        message = "User already exists!!!"
        return render(request, 'Registration.html', {'message': message})
    else:
        if str(userType) == 'Staff':
            create = User.objects.create_user(
                username=email, first_name=userType, last_name='Inactive', password=password)
        else:
            create = User.objects.create_user(
                username=email, first_name=userType, password=password)
        create.save()
        message = "User created Sucessfully"
        return render(request, 'login.html', {'message': message})


def login(request):
    if request.method == 'GET':
        return render(request, 'login.html')
    else:
        username = request.POST['email']
        password = request.POST['password']
        userType = request.POST['userType']
        user = auth.authenticate(
            username=username, first_name=userType, password=password)
        if user is not None:
            auth.login(request, user)
            if str(userType) == 'admin':
                allUsers = User.objects.all()
                return render(request, 'admin_dashboard.html', {'name': username, 'all': allUsers})
            elif str(userType) == 'Staff':
                if user.last_name == 'Inactive':
                    auth.logout(request)
                    return render(request, 'login.html', {'message': 'User not active contact Administrator'})
                else:
                    return render(request, 'staff_dashboard.html', {'name': username})
            else:
                return render(request, 'customer_dashboard.html', {'name': username})
        else:
            message = "Invalid Credentials"
            return render(request, 'login.html', {'message': message})


def logout(request):
    auth.logout(request)
    message = "Logged out successfully"
    return render(request, 'index.html', {'message': message})


def activate(request, id):
    activateObject = User.objects.filter(id=id)
    activateObject.update(last_name='Active')
    allObjects = User.objects.all()
    return render(request, 'admin_dashboard.html', {'all': allObjects})


def deactivate(request, id):
    activateObject = User.objects.filter(id=id)
    activateObject.update(last_name='Inactive')
    allObjects = User.objects.all()
    return render(request, 'admin_dashboard.html', {'all': allObjects})
